# This Python Script is an adaptation of the methods employed
# by Malcolm Kesson
# This Script Calculates the shortest distance from a point
# (NED Offset) to a line Segment (Finish Line)
# The result will return one of three answers:
# The location of the boat is closest to one of the two endpoints of the
# finish line,
# or the shortest distance is the perpendicular distance.
# A full explanation can be found in the report.
# Note the variable conventions are inline with Python 2.x

# As per Malcolm Kessen Instructions:

# Given a line with coordinates 'start' and 'end' and the
# coordinates of a point 'pnt' the proc returns the shortest
# distance from pnt to the line and the coordinates of the
# nearest point on the line.
#
# 1  Convert the line segment to a vector ('line_vec').
# 2  Create a vector connecting start to pnt ('pnt_vec').
# 3  Find the length of the line vector ('line_len').
# 4  Convert line_vec to a unit vector ('line_unitvec').
# 5  Scale pnt_vec by line_len ('pnt_vec_scaled').
# 6  Get the dot product of line_unitvec and pnt_vec_scaled ('t').
# 7  Ensure t is in the range 0 to 1.
# 8  Use t to get the nearest location on the line to the end
#    of vector pnt_vec_scaled ('nearest').
# 9  Calculate the distance from nearest to pnt_vec_scaled.
# 10 Translate nearest back to the start/end line.
# Malcolm Kesson 16 Dec 2012

import math


# Calculate the dot product of two 2D vectors v, w
def dot(v, w):
    # Unpack the vectors into thei x and y coordinates
    x, y = v
    X, Y = w
    return x * X + y * Y


# find the length of a vector from the origin
def length(v):
    x, y = v
    return math.sqrt(x * x + y * y)


# find the difference between two vectors
def vector(b, e):
    x, y = b
    X, Y = e
    return (X - x, Y - y)


# find the unit length of the vector
def unit(v):
    x, y = v
    mag = length(v)
    return x / mag, y / mag


# find the distanec between two points
def distance(p0, p1):
    return length(vector(p0, p1))


# Scale a vector by scaling fact sc
def scale(v, sc):
    x, y = v
    return x * sc, y * sc


# compute vector addition
def add(v, w):
    x, y = v
    X, Y = w
    return x + X, y + Y


def pnt2line(boat_data, base_data):
    pnt = boat_data[0:2]
    start = [0, 0]
    end = base_data[0:2]
    line_vec = vector(start, end)
    pnt_vec = vector(start, pnt)
    line_len = length(line_vec)
    line_unitvec = unit(line_vec)
    pnt_vec_scaled = scale(pnt_vec, 1.0 / line_len)
    t = dot(line_unitvec, pnt_vec_scaled)
    if t < 0.0:
        t = 0.0
    elif t > 1.0:
        t = 1.0
    nearest = scale(line_vec, t)
    dist = distance(nearest, pnt_vec)
    nearest = list(add(nearest, start))

    return dist, boat_data[2]

# Basic function to test script functionality
def test_pnt():
    dist, time = pnt2line([0, -3600, 10000], [6000, 8000])
    print(dist)


if __name__ == "__main__":
    test_pnt()
